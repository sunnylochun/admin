<?php
require '../__admin_required.php';
require '../__connect_db.php';
$page_name = 'restaurant_list';
$page_title = '餐廳列表';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

$per_page = 10; // 每一頁要顯示幾筆

$t_sql = "SELECT COUNT(1) FROM `restaurant` ";

$t_stmt = $pdo->query($t_sql);
$totalRows = $t_stmt->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數
//$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0]; // 拿到總筆數

$totalPages = ceil($totalRows / $per_page); // 取得總頁數

if ($page < 1) {
    header('Location: restaurant_list.php');
    exit;
}
if ($page > $totalPages) {
    header('Location: restaurant_list.php?page=' . $totalPages);
    exit;
}

$sql = sprintf(
    "SELECT * FROM `restaurant` ORDER BY `restaurant_id` DESC LIMIT %s, %s",
    ($page - 1) * $per_page,
    $per_page
);
$stmt = $pdo->query($sql);

//$rows = $stmt->fetchAll();

?>

<?php include  '../__html_head.php' ?>
<?php include  '../__html_body.php' ?>
<div class="content" style="margin-top:-50px;">
    <div style="margin-top: 2rem;">
        <div aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $page - 1 ?>">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                </li>
                <?php
                $p_start = $page - 5;
                $p_end = $page + 5;
                for ($i = $p_start; $i <= $p_end; $i++) :
                    if ($i < 1 or $i > $totalPages) continue;
                    ?>
                    <li class="page-item <?= $i == $page ? 'active' : '' ?>">
                        <a class="page-link" href="?page=<?= $i ?>"><?= $i ?></a>
                    </li>
                <?php endfor; ?>
                <li class="page-item">
                    <a class="page-link" href="?page=<?= $page + 1 ?>">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </div>


        <table class="table table-striped table-bordered">

            <thead>

                <tr>
                    <th scope="col" style="vertical-align:left;">
                        <label class='checkbox-inline checkboxeach'>
                            <input id='checkAll' type='checkbox' name='checkboxall' value='1'></label>選取
                    </th>
                    <th scope="col"><a href="javascript:delete_all()"><i class="fas fa-trash-alt delete_all"></i></a></th>


                    
                    <th scope="col">#</th>
                    <th scope="col">餐廳名</th>
                    <th scope="col">電話</th>
                    <th scope="col">地址</th>
                    <th scope="col">休息日</th>
                    <th scope="col">營業時間</th>
                    <th scope="col">葷素</th>
                    <th scope="col">帳號</th>
                    <th scope="col">密碼</th>
                    <th scope="col">代客烹煮價碼</th>
                    <th scope="col">代客烹煮時間</th>
                    <th scope="col">平均客單價</th>
                    <th scope="col">網站</th>
                    <th scope="col">餐廳圖</th>

                    <th scope="col">套餐品項</th>
                    <th scope="col"><i class="fas fa-edit"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php while ($r = $stmt->fetch()) {  ?>
                    <tr>

                        <td class="box_td">
                            <label class=' checkbox-inline checkboxeach'>
                                <input id="<?= 'readtrue' . $r['restaurant_id'] ?>" type='checkbox' name=<?= 'readtrue' . $r['restaurant_id'] . '[]' ?> value='<?= $r['restaurant_id'] ?>'> <!-- 選取框 -->
                            </label>
                        </td>
                        


                        <td>
                            <a href="javascript:delete_one(<?= $r['restaurant_id'] ?>)"><i class="fas fa-trash-alt"></i></a>
                        </td>
                        <td><?= $r['restaurant_id'] ?></td>
                        <td><?= htmlentities($r['name']) ?></td>
                        <td><?= htmlentities($r['mobile']) ?></td>
                        <td><?= htmlentities($r['address']) ?></td>
                        <td><?= htmlentities($r['holiday']) ?></td>
                        <td><?= htmlentities($r['businesstime']) ?></td>
                        <td><?= htmlentities($r['vegetarian']) ?></td>
                        <td><?= htmlentities($r['user']) ?></td>
                        <td><?= htmlentities($r['password']) ?></td>
                        <td><?= htmlentities($r['cook']) ?></td>
                        <td><?= htmlentities($r['cooktime']) ?></td>
                        <td><?= htmlentities($r['pct']) ?></td>
                        <td><?= htmlentities($r['website']) ?></td>
                        <td><img src="<?= 'uploads/' . $r['my_file'] ?>" alt=""></td>

                        <td><?php
                                $a = json_decode($r['setoption'], JSON_UNESCAPED_UNICODE);
                                if (isset($a)) {

                                    foreach ($a as $b) {

                                        echo  "$b" . " ,";
                                    }
                                }

                                ?></td>
                        <td>
                            <a href="restaurant_edit.php?restaurant_id=<?= $r['restaurant_id'] ?>"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                <?php } ?>


            </tbody>
        </table>
    </div>

    <script>
        let checkAll = $('#checkAll'); //控制所有勾選的欄位
        let checkBoxes = $('tbody .checkboxeach input'); //其他勾選欄位
        


        checkAll.click(function() {
            for (let i = 0; i < checkBoxes.length; i++) {
                checkBoxes[i].checked = this.checked;
            }
        })


       
    </script>

    <script>
        function delete_one(restaurant_id) {
            if (confirm(`確定要刪除編號為 ${restaurant_id} 的資料嗎?`)) {
                location.href = 'restaurant_delete.php?restaurant_id=' + restaurant_id;
            }
        }

        function delete_all() {
            let sids = [];
            checkBoxes.each(function() {
                if ($(this).prop('checked')) {
                    sids.push($(this).val())
                }
            });
            if (!sids.length) {
                alert('沒有選擇任何資料');
            } else {
                if(confirm('確定要刪除這些資料嗎？')){
                    location.href = 'data_delete_all.php?sids=' + sids.toString();
                }

            }
        }

    </script>
</div>





<?php include  '../__html_foot.php' ?>